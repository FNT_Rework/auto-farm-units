import { DotaMap, MapArea, Unit, Vector3 } from "../wrapper/Imports";

export class LocationFarmX  {

	public IsJungle(pos: Vector3) {
		const area = DotaMap.GetMapArea(pos)
		return area[0] === MapArea.TopJungle || area[0] === MapArea.BottomJungle
	}

	public GetLane(hero: Unit): MapArea {
		const area = DotaMap.GetMapArea(hero.Position)
		switch (area[0]) {
			case MapArea.Top:
				return MapArea.Top
			case MapArea.Middle:
				return MapArea.Middle
			case MapArea.Bottom:
				return MapArea.Bottom
			case MapArea.TopJungle:
				return MapArea.Top
			case MapArea.BottomJungle:
				return MapArea.Bottom
			default:
				return MapArea.Middle
		}
	}

	public GetNextPoint(hero: Unit): Nullable<Vector3> {
		return DotaMap.GetCreepCurrentTarget(hero.Position, hero.Team, this.GetLane(hero))?.Position
	}
}
