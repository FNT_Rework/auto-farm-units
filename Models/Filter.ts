import { ArrayExtensions, Creep, Hero, Tower, Unit } from "wrapper/Imports"
import { EntityX } from "../X-Core/Imports"

export class FilterUnits {

	public static Building(source: Unit) {
		return EntityX.EnemyBuilding.find(unit => !(unit instanceof Tower)
			&& this.IsValidOrbwalkingTarget(source, unit)
			&& unit.Name !== "portrait_world_unit")
	}

	public static Creeps(source: Unit): Nullable<Creep> {
		return ArrayExtensions.orderBy(EntityX.EnemyCreeps.filter(unit => unit.IsVisible
			&& unit.IsAlive
			&& unit.IsSpawned
			&& (this.IsValidOrbwalkingTarget(source, unit) || source.Distance2D(unit) <= 500),
		), x => x.IsLaneCreep ? x.HP : x.MaxHP)[0]
	}

	public static Creeps2(source: Unit) {
		return EntityX.EnemyCreeps.find(unit => unit.IsLaneCreep
			&& unit.IsSpawned
			&& this.IsValidOrbwalkingTarget(source, unit),
		)
	}

	public static Towers(source: Unit) {
		return EntityX.EnemyTowers.find(unit => unit.IsAlive
			&& unit.IsVisible
			&& !unit.IsInvulnerable
			&& this.IsInAttackRange(source, unit))
	}

	public static Units(source: Unit) {
		return EntityX.EnemyUnits.find(unit => !(unit instanceof Hero)
			&& !(unit instanceof Creep)
			&& unit.IsAlive
			&& !unit.IsInvulnerable
			&& this.IsValidOrbwalkingTarget(source, unit) && (unit.ClassName !== "CDOTA_Ability_Techies_RemoteMines"))
	}

	private static IsInAttackRange(source: Unit, target: Unit, bonusAttackRange: number = 0) {
		return source.IsInRange(target, source.AttackRange + bonusAttackRange)
	}

	private static IsValidOrbwalkingTarget(attacker: Unit, target: Unit, bonusAttackRange: number = 0) {
		return target.IsVisible
			&& target.IsAlive
			&& !target.IsIllusion
			&& this.IsInAttackRange(attacker, target, bonusAttackRange)
			&& !target.IsInvulnerable && !target.IsAttackImmune
	}
}
