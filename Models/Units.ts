import { ArrayExtensions, Hero, NeutralSpawnerType, Unit, Vector3 } from "wrapper/Imports"
import { AUTO_FARM_DATA, FarmTypeEnum } from "../data"
import { ControllerMode } from "../Service/Controller"
import { EntityX, GameX } from "../X-Core/Imports"
import { FilterUnits } from "./Filter"

export class Base {
	public Id: Nullable<string>
	public FarmType = FarmTypeEnum.Unknown

	constructor(public owner: Hero, public controller: ControllerMode) {
		this.Id = "ControllerMode_" + this.owner.Index
	}

	public Target(hero: Unit) {

		const creep = FilterUnits.Creeps(hero)
		if (creep !== undefined)
			return creep

		const tower = FilterUnits.Towers(hero)
		if (tower !== undefined)
			return tower

		const building = FilterUnits.Building(hero)
		if (building !== undefined)
			return building

		const creep2 = FilterUnits.Creeps2(hero)
		if (creep2 !== undefined)
			return creep2

		const unit = FilterUnits.Units(hero)
		if (unit !== undefined)
			return unit
	}

	public LaneFarm() {
		const target = this.Target(this.owner)
		if (target !== undefined) {
			this.AttackTarget(target)
			return
		}

		const nextPoint = this.controller.base.LocationFarm.GetNextPoint(this.owner)
		if (nextPoint === undefined)
			return
		this.MoveToPosition(nextPoint)
	}

	public JungleFarm() {

		const array_map = [...EntityX.Camps.values()]
		let camp = ArrayExtensions.orderBy(array_map.filter(x => (x.Owners.length < this.controller.IllusionsOnCamp ||  x.Owners.includes(this.owner))
			&& !x.IsEmpty
			&& (this.controller.FarmEnemyCamp || x.IsAlly)
			&& (this.controller.FarmAncients || x.CampType !== NeutralSpawnerType.Ancient),
		), x => x.Position.Distance2D(this.owner.Position))[0]

		if (camp === undefined)
			camp = ArrayExtensions.orderBy(array_map.filter(x => !x.IsEmpty
				&& (this.controller.FarmAncients || x.CampType !== NeutralSpawnerType.Ancient)
				&& (this.controller.FarmEnemyCamp || x.IsAlly)), x => x.Position.Distance2D(this.owner.Position))[0]

		if (camp !== undefined) {
			if (!camp.Owners.includes(this.owner))
				camp.Owners.push(this.owner)

			const target = this.Target(this.owner)

			if (target === undefined && this.owner.Distance2D(camp.Position) <= 100) {
				camp.IsEmpty = true
				return
			}

			if (target !== undefined) {
				this.AttackTarget(target)
				return
			}

			this.MoveToPosition(camp.Position)
		}
	}

	private AttackTarget(target: Unit) {
		if (AUTO_FARM_DATA.UsrBlockOrder)
			return
		const last_owner = this.controller.ListOfTargets.get(this.owner)
		if (last_owner === undefined) {
			this.owner.AttackTarget(target)
			this.controller.ListOfTargets.set(this.owner, [target, true])
			return
		}
		if (!last_owner[0].IsAlive || !last_owner[0].IsVisible) {
			this.controller.ListOfTargets.delete(this.owner)
			return
		}
		if (last_owner[1])
			return
		this.owner.AttackTarget(target)
	}

	private MoveToPosition(vector: Vector3) {
		if (AUTO_FARM_DATA.UsrBlockOrder || this.owner.IsMoving || this.controller.GameSleeperX.Sleeping(`FARM_UNIT_MOVE_${this.owner.Index}`))
			return
		this.owner.AttackMove(vector)
		this.controller.GameSleeperX.Sleep(300 + (GameX.Ping / 1000), `FARM_UNIT_MOVE_${this.owner.Index}`)
	}
}

export class IllusionUnit extends Base {
	constructor(public Owner: Hero, public controller: ControllerMode) {
		super(Owner, controller)
	}
}
