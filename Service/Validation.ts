import { GameX } from "../../X-Core/Imports"
import { State } from "../menu"

export class AUTO_FARM_VALIDATE {
	public static get IsInGame() {
		return State.value && GameX.IsInGame
	}
}
