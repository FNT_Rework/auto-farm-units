import { Color, EventsSDK, RendererSDK, Vector2 } from "wrapper/Imports"
import { AUTO_FARM_VALIDATE } from "../Service/Validation"
import { EntityX } from "../X-Core/Imports"

EventsSDK.on("Draw", () => {
	if (!AUTO_FARM_VALIDATE.IsInGame || !(globalThis as any).KISS)
		return

	EntityX.Camps.forEach((spawner, spawner_name) => {
		const screen_position = RendererSDK.WorldToScreen(spawner.Position)
		if (screen_position !== undefined) {
			const pos = screen_position.Clone()
			const color = new Color(240, 248, 255)
			const text: [string, string | boolean | number][] = [
				["IsAlly: ", spawner.IsAlly],
				["IsEmpty: ", spawner.IsEmpty],
				["Owners: ", spawner.Owners.length],
				["Creeps: ", spawner.Creeps.length],
				["Name: ", spawner_name],
			]

			let i = 0
			text.forEach(([name, value]) => {
				const pos_ = new Vector2(pos.x, pos.y + (i * 15))
				RendererSDK.Text(name, pos_, color, RendererSDK.DefaultFontName, 15)
				RendererSDK.Text(
					` [${value.toString()}]`,
					pos_.AddScalarX(RendererSDK.GetTextSize(name, RendererSDK.DefaultFontName, 15).x),
					color,
					RendererSDK.DefaultFontName,
					15
				)
				i++
			})
		}
	})
})
