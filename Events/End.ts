import { EventsX } from "../../X-Core/Imports"
import { AUTO_FARM_DATA } from "../data"

EventsX.on("GameEnded", () => {
	AUTO_FARM_DATA.Dispose()
})
