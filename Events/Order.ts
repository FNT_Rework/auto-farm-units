import { AUTO_FARM_DATA } from "../data";
import { AUTO_FARM_VALIDATE } from "../Service/Validation"
import { InputEventSDK, VMouseKeys } from "../wrapper/Imports";

const IsKey = (key: VMouseKeys) => {
	return key === VMouseKeys.MK_RBUTTON
		|| key === VMouseKeys.MK_MBUTTON
		|| key === VMouseKeys.MK_LBUTTON
}

InputEventSDK.on("MouseKeyUp", key => {
	if (!AUTO_FARM_VALIDATE.IsInGame || !IsKey(key))
		return true
	AUTO_FARM_DATA.UsrBlockOrder = false
})

InputEventSDK.on("MouseKeyDown", key => {
	if (!AUTO_FARM_VALIDATE.IsInGame  || !IsKey(key))
		return true
	AUTO_FARM_DATA.UsrBlockOrder = true
})
